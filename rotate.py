import os
import sys
import platform
import subprocess
import serial
import serial.tools.list_ports

#SERIAL_PORT = sys.argv[1]
ports = serial.tools.list_ports.comports(include_links=False)
for port in ports :
    SERIAL_PORT = port.device
#BAUD_RATE =  sys.argv[2]
BAUD_RATE =  9600

ser = serial.Serial(SERIAL_PORT, BAUD_RATE, timeout= None)

while True:
    angle = int(ser.readline().decode('utf-8'))
    if platform.system() == "Windows":
        os.system("display.exe /rotate:%i" % angle)
    else:
        output =  subprocess.check_output("xrandr | grep \" connected\" | awk '{print$1}' ", shell=True).decode('utf-8')
        if angle == 0:
            os.system("xrandr --output %s --rotate normal" % output.rstrip())
        elif angle == 90:
            os.system("xrandr --output %s --rotate left" % output.rstrip())
        elif angle == 270:
            os.system("xrandr --output %s --rotate right" % output.rstrip())
        else:
            os.system("xrandr --output %s --rotate inverted" % output.rstrip())
